defmodule L5rWeb.Schema do
  use Absinthe.Schema
  use Timex
  alias L5rWeb.Resolvers

  query do
    @desc "The list of all Legend of the Five Rings (LCG) cards"
    field :cards, list_of(:card) do
      arg :filter, :card_filter
      arg :order, :sort_order
      resolve &Resolvers.Card.cards/3
    end
  end

  object :card do
    field :id, :id
    field :clan, :string
    field :cost, :integer
    field :deck_limit, :integer
    field :glory, :integer
    field :string_id, :string
    field :influence_cost, :integer
    field :military, :integer
    field :military_bonus, :string
    field :name, :string
    field :name_cannonical, :string
    field :political, :integer
    field :political_bonus, :string
    field :side, :string
    field :strength, :integer
    field :strength_bonus, :string
    field :text, :string
    field :text_canonical, :string
    field :type, :string
    field :unicity, :boolean
    field :inserted_at, :time
    field :updated_at, :time
  end

  @desc "Filtering options for the card list"
  input_object :card_filter do

    @desc "Matching a name"
    field :name, :string

    @desc "Matching a clan"
    field :clan, :string

    @desc "Matching a cost"
    field :cost, :integer

    @desc "Matching a side"
    field :side, :string

    @desc "Cost above a value"
    field :cost_above, :integer

    @desc "Cost below a value"
    field :cost_below, :integer

    @desc "Matching a pack"
    field :pack, :string

  end

  scalar :time do
    description "Time (in ISOz format)"
    parse &Timex.parse(&1, "{ISO:Extended:Z}.")
    serialize &Timex.format!(&1, "{ISO:Extended:Z}")
  end

  enum :sort_order do
    value :asc
    value :desc
  end
end
