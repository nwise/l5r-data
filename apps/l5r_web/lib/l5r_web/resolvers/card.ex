defmodule L5rWeb.Resolvers.Card do
  alias L5rData.{Repo, Card}

  def cards(_, args, _) do
    results =
      args
      |> Card.list_cards
      |> Repo.all
    {:ok, results}
  end
end
