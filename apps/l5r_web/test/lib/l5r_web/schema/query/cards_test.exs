require IEx
defmodule L5rWeb.Schema.Query.Card do
  use L5rWeb.ConnCase, async: true
  import L5rWeb.Factory

  @query """
  {
    cards {
      name
    }
  }
  """
  test "cards field returns cards", %{conn: conn} do
    insert_list(5, :card, %{})

    conn = get conn, "/api", query: @query
    response = json_response(conn, 200)

    assert length(response["data"]["cards"]) == 5
  end

  @query """
  {
    cards(filter: {name: "found it"}) {
      name
    }
  }
  """
  test "cards field returns cards filtered by name", %{conn: conn} do
    insert(:card, %{name: "a card called found it"})
    insert(:card, %{name: "a card called that won't be found"})

    conn = get conn, "/api", query: @query
    response = json_response(conn, 200)

    assert [%{"name" => "a card called found it"}] = response["data"]["cards"]
  end

  @query """
  {
    cards(filter: {name: 123}) {
      name
    }
  }
  """
  test "cards field returns errors when using a bad value", %{conn: conn} do
    conn = get conn, "/api", query: @query

    assert %{"errors" =>[
             %{"message" => message}
           ]} = json_response(conn, 400)
    assert message == "Argument \"filter\" has invalid value {name: 123}.\nIn field \"name\": Expected type \"String\", found 123."
  end

  @query """
  query ($term: String) {
    cards(filter: {name: $term}){
      name
    }
  }
  """
  @variables %{"term" => "found it"}
  test "cards field filters by name when using a variable", %{conn: conn} do
    insert(:card, %{name: "a card called found it"})
    insert(:card, %{name: "a card called that won't be found"})

    conn = get(conn, "/api", query: @query, variables: @variables)
    response = json_response(conn, 200)

    assert [%{"name" => "a card called found it"}] = response["data"]["cards"]
  end

  @query """
  {
    cards(order: DESC) {
      name
    }
  }
  """
  test "cards field returns items descending using literals", %{conn: conn} do
    expected_first = insert(:card, %{name: "B: First"})
    expected_last = insert(:card, %{name: "A: Last"})

    conn = get(conn, "/api", query: @query)
    %{"data" => %{"cards" => [first, last]}} = json_response(conn, 200)

    assert expected_first.name == first["name"]
    assert expected_last.name == last["name"]
  end

  @query """
  query ($order: SortOrder!) {
    cards(order: $order) {
      name
    }
  }
  """
  @variables %{"order" => "DESC"}

  test "cards field returns items descending using variables", %{conn: conn} do
    expected_first = insert(:card, %{name: "B: First"})
    expected_last = insert(:card, %{name: "A: Last"})

    conn = get(conn, "/api", query: @query, variables: @variables)
    %{"data" => %{"cards" => [first, last]}} = json_response(conn, 200)

    assert expected_first.name == first["name"]
    assert expected_last.name == last["name"]
  end

  @query """
  {
    cards(filter: {cost_above: 2}) {
      name
    }
  }
  """

  test "cards field returns cards, filtering with a literal", %{conn: conn} do
    expected = insert(:card, %{name: "Found", side: "conflict", cost: 2})
    insert(:card, %{name: "Not Found", side: "conflict", cost: 1})

    conn = get(conn, "/api", query: @query, variables: @variables)
    %{"data" => %{"cards" => [card]}} = json_response(conn, 200)

    assert expected.name == card["name"]
  end


end
