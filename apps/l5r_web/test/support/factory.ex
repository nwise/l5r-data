defmodule L5rWeb.Factory do
  use ExMachina.Ecto, repo: L5rData.Repo

  def card_factory do
    %L5rData.Card{
      clan: Faker.Team.name(),
      cost: Faker.random_between(0, 5),
      deck_limit: 4,
      glory: Faker.random_between(0, 3),
      influence_cost: Faker.random_between(0, 3),
      military: Faker.random_between(0, 3),
      military_bonus: "+#{Faker.random_between(0,2)}",
      name: Faker.Pokemon.name(),
      political: Faker.random_between(0, 3),
      political_bonus: "-#{Faker.random_between(0,2)}",
      side: sequence(:role, ["dynasty", "conflict", "province", "role"]),
      text: Faker.Lorem.sentence()
    }
  end

  #def article_factory do
    #title = sequence(:title, &"Use ExMachina! (Part #{&1})")
    ## derived attribute
    #slug = MyApp.Article.title_to_slug(title)
    #%MyApp.Article{
      #title: title,
      #slug: slug,
      ## associations are inserted when you call `insert`
      #author: build(:user),
    #}
  #end

  # derived factory
  #def featured_article_factory do
    #struct!(
      #article_factory(),
      #%{
        #featured: true,
      #}
    #)
  #end

  #def comment_factory do
    #%MyApp.Comment{
      #text: "It's great!",
      #article: build(:article),
    #}
  #end
end
