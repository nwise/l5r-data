defmodule L5rWeb.PageController do
  use L5rWeb.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
