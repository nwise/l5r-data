defmodule L5rWeb.Router do
  use L5rWeb.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    forward "/api", Absinthe.Plug, schema: L5rWeb.Schema
    forward "/graphiql", Absinthe.Plug.GraphiQL, schema: L5rWeb.Schema, interface: :simple
  end

  scope "/", L5rWeb do
    pipe_through :api # Use the default browser stack

    get "/", PageController, :index
  end
end
