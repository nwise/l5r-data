defmodule L5rData.Repo.Migrations.AddCycles do
  use Ecto.Migration

  def change do
    create table(:cycles, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :position, :int
      add :size, :int

      timestamps()
    end
    create unique_index(:cycles, [:name])
  end
end
