defmodule L5rData.Repo.Migrations.AddCardsTraitsTable do
  use Ecto.Migration

  def change do
    create table(:cards_traits, primary_key: false) do
      add :card_id, references(:cards, type: :binary_id, on_delete: :delete_all)
      add :trait_id, references(:traits, type: :binary_id, on_delete: :delete_all)
    end

    create unique_index(:cards_traits, [:card_id, :trait_id])
  end
end
