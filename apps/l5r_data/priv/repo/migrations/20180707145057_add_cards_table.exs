defmodule L5rData.Repo.Migrations.AddCardsTable do
  use Ecto.Migration

  def change do
    create table(:cards, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :clan, :string
      add :cost, :int
      add :deck_limit, :int
      add :glory, :int
      add :string_id, :string
      add :influence_cost, :int
      add :military, :int
      add :military_bonus, :string
      add :name, :string
      add :name_cannonical, :string
      add :political, :int
      add :political_bonus, :string
      add :side, :string
      add :strength, :int
      add :strength_bonus, :string
      add :text, :text
      add :text_canonical, :text
      add :type, :string
      add :unicity, :boolean
      add :trait_id, :uuid
      timestamps()
    end

    create unique_index(:cards, [:string_id])
  end
end
