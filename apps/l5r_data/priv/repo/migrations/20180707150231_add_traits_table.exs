defmodule L5rData.Repo.Migrations.AddConditionsTable do
  use Ecto.Migration

  def change do
    create table(:traits, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string

      timestamps()
    end
    create unique_index(:traits, [:name])
  end
end
