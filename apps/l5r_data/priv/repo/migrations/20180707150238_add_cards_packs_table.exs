defmodule L5rData.Repo.Migrations.AddCardsPacksTable do
  use Ecto.Migration

  def change do
    create table(:cards_packs, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :card_id, references(:cards, type: :uuid, on_delete: :delete_all)
      add :pack_id, references(:packs, type: :uuid, on_delete: :delete_all)
      add :flavor, :text
      add :illustrator, :string
      add :image_url, :string
      add :position, :string
      add :quantity, :int
    end
    create unique_index(:cards_packs, [:card_id, :pack_id])
  end
end
