defmodule L5rData.Repo.Migrations.AddPacksTable do
  use Ecto.Migration

  def change do
    create table(:packs, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :short_name, :string
      add :position, :int
      add :size, :int
      add :cycle_id, references(:cycles, type: :uuid, on_delete: :delete_all)

      timestamps()
    end
    create unique_index(:packs, [:name])
  end
end
