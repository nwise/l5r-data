use Mix.Config

config :l5r_data, L5rData.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "l5r_data_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  port: "5432",
  pool: Ecto.Adapters.SQL.Sandbox
