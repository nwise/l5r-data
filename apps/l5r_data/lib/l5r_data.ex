defmodule L5rData do
  @moduledoc """
  Documentation for L5rData.
  """

  @doc """
  Hello world.

  ## Examples

      iex> L5rData.hello
      :world

  """
  def hello do
    :world
  end
end
