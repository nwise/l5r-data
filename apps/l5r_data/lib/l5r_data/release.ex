defmodule L5rData.Release do
  use Ecto.Schema
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "cards_packs" do
    field :flavor, :string
    field :illustrator, :string
    field :image_url, :string
    field :position, :string
    field :quantity, :integer

    belongs_to :card, L5rData.Card, foreign_key: :card_id, type: :binary_id
    belongs_to :pack, L5rData.Pack, foreign_key: :pack_id, type: :binary_id
  end

  @required_fields ~w()
  @optional_fields ~w(flavor image_url illustrator position quantity)

  def changeset(data) do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
    |> Ecto.Changeset.put_assoc(:pack, data["pack"])
    |> Ecto.Changeset.put_assoc(:card, data["card"])
  end

end
