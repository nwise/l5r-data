defmodule L5rData.Repo do
  use Ecto.Repo, otp_app: :l5r_data,
    adapter: Ecto.Adapters.Postgres
end
