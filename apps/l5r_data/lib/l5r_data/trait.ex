require IEx
defmodule L5rData.Trait do
  use Ecto.Schema
  import Ecto.Changeset
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "traits" do
    field :name, :string

    timestamps()

    many_to_many :cards, L5rData.Card, join_through: "cards_traits"
  end

  @required_fields ~w(name)
  @optional_fields ~w()

  def changeset(data) do
    __struct__()
    |>  Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
  end
end
