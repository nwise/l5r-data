require IEx
defmodule L5rData.Pack do
  use Ecto.Schema
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "packs" do
    field :name, :string
    field :short_name, :string
    field :position, :integer
    field :size, :integer

    belongs_to :cycle, L5rData.Cycle, foreign_key: :cycle_id, type: :binary_id

    timestamps()
  end

  @required_fields ~w(name short_name position size)
  @optional_fields ~w()

  def changeset(data = %{"cycle" => cycle}) do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
    |> Ecto.Changeset.put_assoc(:cycle, cycle)
  end
  def changeset(data) do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
  end
end
