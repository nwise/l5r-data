defmodule L5rData.Cycle do
  use Ecto.Schema
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "cycles" do
    field :name, :string
    field :position, :integer
    field :size, :integer

    timestamps()
  end

  @required_fields ~w(name position size)

  def changeset(data) do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields)
  end

end
