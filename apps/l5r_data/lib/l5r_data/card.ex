require IEx
defmodule L5rData.Card do
  use Ecto.Schema
  import Ecto.Query
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "cards" do
    field :clan, :string
    field :cost, :integer
    field :deck_limit, :integer
    field :glory, :integer
    field :string_id, :string
    field :influence_cost, :integer
    field :military, :integer
    field :military_bonus, :string
    field :name, :string
    field :name_cannonical, :string
    field :political, :integer
    field :political_bonus, :string
    field :side, :string
    field :strength, :integer
    field :strength_bonus, :string
    field :text, :string
    field :text_canonical, :string
    field :type, :string
    field :unicity, :boolean

    timestamps()

    many_to_many :traits, L5rData.Trait, join_through: "cards_traits"
    many_to_many :packs, L5rData.Pack, join_through: "cards_packs"
    has_many :releases, L5rData.Release
  end

  @required_fields ~w(name)
  @optional_fields ~w(clan cost deck_limit string_id influence_cost
    name name_cannonical side text text_canonical type unicity glory
    military military_bonus political political_bonus strength)

  def changeset(data = %{"traits" => traits}) when length(traits) > 0 do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
    |> Ecto.Changeset.put_assoc(:traits, traits)
    |> Ecto.Changeset.put_assoc(:releases, data["releases"])
  end

  def changeset(data) do
    __struct__()
    |> Ecto.Changeset.cast(data, @required_fields ++ @optional_fields)
    |> Ecto.Changeset.put_assoc(:releases, data["releases"])
  end

  def list_cards(filters) do
    filters
    |> Enum.reduce(__MODULE__, fn
      {_, nil}, query ->
        query
      {:order, order}, query ->
        from q in query, order_by: {^order, :name}
      {:filter, filter}, query ->
        query |> filter_with(filter)
    end)
  end

  def filter_with(query, filter) do
    Enum.reduce(filter, query, fn
      {:name, name}, query ->
        from q in query, where: ilike(q.name, ^"%#{name}%")
      {:cost, cost}, query ->
        from q in query, where: q.cost == ^cost
      {:clan, clan}, query ->
        from q in query, where: q.clan == ^clan
      {:side, side}, query ->
        from q in query, where: q.side == ^side
      {:cost_above, cost}, query ->
        from q in query, where: q.cost >= ^cost
      {:cost_below, cost}, query ->
        from q in query, where: q.cost <= ^cost
      {:pack, pack}, query ->
        from q in query,
          join: p in assoc(q, :packs),
          where: ilike(p.name, ^"%#{pack}%")
    end)
  end

end
