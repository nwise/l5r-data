require IEx
defmodule Mix.Tasks.LoadCards do
  use Mix.Task
  import Mix.Ecto
  alias L5rData.{DataImporter, Repo, Card, Trait, Pack, Release, Cycle}

  @shortdoc "Loads cards from cards.json file"
  @moduledoc ~S"""

  """
  def run(path) do
    # Load JSON
    ensure_started(L5rData.Repo, [])

    [Card, Trait, Pack, Release, Cycle]
    |> Enum.each(&(Repo.delete_all(&1)))

    [:cycles, :packs, :cards]
    |> Enum.each(fn(type) ->
      IO.puts "Loading #{type}"
        case File.read("#{path}/#{type}.json") do
          {:ok, json} ->
            Jason.decode!(json)
            |> Map.fetch!("records")
            |> DataImporter.persist(type)
          {:error, :enoent} ->
            Mix.shell.error "Invalid File - #{type}"
        end
    end)

  end
end

defmodule L5rData.DataImporter do
  alias L5rData.{Repo, Card, Trait, Pack, Release, Cycle}

  def persist(cards, :cards) do
    # Loop Cards
    Enum.each(cards, fn(card) ->
      # Insert Cards
      # Create/Assoc Releases
      card
      |> Map.merge(%{"traits" => get_or_create_traits(card["traits"])})
      |> Map.merge(%{"releases" => get_or_create_releases(card["pack_cards"])})
      |> Card.changeset
      |> Repo.insert!
    end)
  end

  def get_or_create_releases(nil), do: []
  def get_or_create_releases([]), do: []
  def get_or_create_releases(releases) do
    Enum.reduce(releases, [], fn(release, acc) ->
      pack = Repo.get_by(Pack, %{short_name: release["pack"]["id"]})
      r =
        release
        |> Map.merge(%{"pack" => pack})
        |> Release.changeset
        |> Repo.insert!
      acc ++ [r]
    end)
  end

  def get_or_create_traits(nil), do: []
  def get_or_create_traits([]), do: []
  def get_or_create_traits(traits) do
    Enum.reduce(traits, [], fn(trait, acc) ->
      existing_trait = Repo.get_by(Trait, %{name: trait})
      case existing_trait do
        nil ->
          acc ++ [Repo.insert!(Trait.changeset(%{"name" => trait}))]
        _ -> acc ++ [existing_trait]
      end
    end)
  end
  def persist(packs, :packs) when is_nil(packs), do: nil
  def persist(packs, :packs) do
    Enum.each(packs, fn(pack) ->
      pack
      |> Map.merge(%{"cycle" => get_cycle_for_pack(pack), "short_name" => pack["id"] })
      |> Pack.changeset
      |> Repo.insert!
    end)
  end
  def persist(cycles, :cycles) when is_nil(cycles), do: nil
  def persist(cycles, :cycles) do
    Enum.each(cycles, fn(cycle) ->
      cycle
      |> Map.merge(%{"name" => cycle["id"]})
      |> Cycle.changeset
      |> Repo.insert!
    end)
  end

  def get_cycle_for_pack(pack) do
    L5rData.Repo.get_by(Cycle, %{name: pack["cycle"]["id"]})
  end

  def debug(x) do
    IEx.pry
    x
  end
end
