defmodule L5rData.CardTrait do
  use Ecto.Schema
  #@primary_key {:id, :binary_id, autogenerate: true}

  schema "cards_traits" do
    belongs_to :card, L5rData.Card, foreign_key: :card_id, type: :binary_id
    belongs_to :trait, L5rData.Trait, foreign_key: :trait_id, type: :binary_id
  end

end

