# L5rData

[![pipeline status](https://gitlab.com/nwise/l5r-data/badges/master/pipeline.svg)](https://gitlab.com/nwise/l5r-data/commits/master)

## Dev Installation

1. `brew install elixir`
2. `mix deps.get`
3. `mix compile`
4. `cp apps/l5r_data/config/dev.secrets.exs.example apps/l5r_data/config/dev.secrets.exs` and update creds for local DB.
5. `mix ecto.create`
6. `mix ecto.migrate`
7. To load the cards do: `mix load_cards apps/l5r_data`

## Run API
1. `mix phx.server`
2. Open your browser to `http://localhost:4000/graphiql`

## Run Tests
1. Enusre `postres` user with password specified in the config (`apps/l5r_data/config/test.secrets.exs`) exists.
2. `MIX_ENV=test mix ecto.create`
3. `MIX_ENV=test mix ecto.migrate`
4. `mix test`

## Run Interactive Console
1. `iex -S mix`
2. Fetch all Cards: `L5rData.Repo.all(L5rData.Card)`
