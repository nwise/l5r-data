use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: :dev

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html


# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile

environment :dev do
  set dev_mode: true
  set include_erts: false
  set cookie: :"wfS7}KiW7%m_Sw.:lc%o^C0SHQ~b3S:Vr>;WB^z,=!j84Jp1!mz/et~m`$1(q^ot"
end

environment :prod do
  set include_erts: true
  set include_src: false
  set cookie: :"M{,mEr3BMch6k%:31b,RQws&8r(u*rINiOt~fy)p|B^(]&%<3Ld=A}BS<jkBCAt:"
end

# You may define one or more releases in this file.
# If you have not set a default release, or selected one
# when running `mix release`, the first release in the file
# will be used by default

release :l5r_search do
  set version: "0.1.0"
  set applications: [
    l5r_data: :permanent,
    l5r_web: :permanent
  ]
end

